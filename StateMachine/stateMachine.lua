fsm = require 'pluto.fsm'
eloop = require 'pluto.eloop'

-- Ingimundur's hopper fsm

-- Register switches for the Fill and Ready states
switchSlot = DIOSlot:new()
switchSlot:register("switchSlot")

switch = DIOSignal:new()
switch:register("switch")

switch:set(true)

-- Initialize the counter to 0
--counter_signal:set(0)

-- Make an fsm
state_machine = {
	name = "state_machine",
	instance = 1
}

state_machine = fsm.new(state_machine)

-- Add the fill state
state_machine:_add{
	name = "On",
	
	ea = function(self)
		self.lastState = self.Off
		print("---------- Entered State: On ----------")
		print("ea : On ")
	end,
	
	tc = function(self)
		if not switchSlot:get() then
			print("tc : On ")
			return self.Off
		end
		
	end,
	
	xa = function(self)
		print("xa : On ")
		print("Exited State: On\n")
	end,
}

-- Add the ready state
state_machine:_add{
	name = "Off",
	
	ea = function(self)
		self.lastState = self.On
		print("---------- Entered State: Off ----------")
		print("ea : Off ")
	end,
	
	tc = function(self)
		
		if switchSlot:get() then
			print("tc : Off ")
			return self.On
		end
		-- If the ready switch has been toggled, transition to the release state

	end,
	
	xa = function(self)
		print("xa: Off ")
		print("Exited State: Off\n")
	end,
}

-- When the counter is incremented, a step is made in the fsm
function switchSlot:newstate(state)
	state_machine:_run()
	--print(state_machine._state.name)
end