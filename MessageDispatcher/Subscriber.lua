#!/usr/bin/iolua
 
local md = require "pluto.mdc"
local eloop = require("pluto.eloop")

local m1 = md:create()
local m2 = md:create()

local timer

function printer(t, tab)
    for k,v in pairs(t) do
        if type(v) == "table" then
            print(tab .. k)
            printer(v,tab.."   ")
        elseif(type(v) == "userdata") or (type(v) == "function") then
            print(tab.."[ " .. k .. " ] type: " .. type(v))
        elseif(type(v) == "boolean") then
            print(tab.."[ " .. k .. " ] = " .. (v and "true" or "false"))
            print("blabla")
        else
            print(tab.."[ " .. k .. " ] = " .. v)        
        end       
    end
end
 
function callback(topic, message)   
    print("Lua subscribe callback")
    print("Received topic: " .. topic)   
    printer(message, "")
end
 
m1:subscribe("topic1", callback)
m1:subscribe("topic2", callback) 

function timeout_1(self)
    print("========Unsubscribing topic1, m1===========")
    m1:unsubscribe("topic2")
end
 
timer = eloop.new_timer(5000, false, timeout_1)