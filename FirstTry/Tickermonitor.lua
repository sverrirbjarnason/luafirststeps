#!/usr/bin/iolua
-- Copyright (C) 2015 Marel hf. All rights reserved.
local eloop = require 'pluto.eloop'
local sm = require 'pluto.sharedmemory'
local TM ={}

function TM:new(FriendlyName, NameOfTicker, CheckIntervalMs)
	local o = {}
    setmetatable(o, self)
    self.__index = self
    o.FriendlyName = FriendlyName
	o.NameOfTicker = NameOfTicker
	o.CheckIntervalMs = CheckIntervalMs or 2000
	o:SetInitalConditions()
	o:CreatSlots()
	o:CreatSignals()	
	o.Tacho = Ticker:open(o.NameOfTicker)
 	o.Run.newstate = function(self, state) o.Runing = state end	
 	o.QSPpin.newstate = function(self, state) o.QSP = state end	
	o:createEloop()
	return o
end	

function TM:SetInitalConditions()
	self.Runing = false
	self.QSP = false
end

function TM:CreatSignals()	
	self.Incrementing = DIOSignal:new()
	self.Incrementing:register(InsetName(self.FriendlyName).."Incrementing")
	self.Stable = DIOSignal:new()
	self.Stable:register(InsetName(self.FriendlyName).."Stable")
	self.Alarm = DIOSignal:new()
	self.Alarm:register(InsetName(self.FriendlyName).."Alarm")
end

function InsetName(Name)
	if Name and Name ~= "" then 
		return Name .. "_"
	end
	return ""
end

function TM:CreatSlots()
	self.Run = DIOSlot:new()
	self.Run:register(InsetName(self.FriendlyName).."Run")
	self.QSPpin = DIOSlot:new()
	self.QSPpin:register(InsetName(self.FriendlyName).."QSP")
end

function TM:createEloop()
	self.elooptimer = eloop.new_timer(self.CheckIntervalMs, true, function() self:CheckForError() end )
end

function TM:CheckForError() 
	if self:IsConveyorRunning() then 
		self:SetOutputsRunning()
	else
		self:SetOutputsStopped()
	end
end

function TM:SetOutputsStopped()
	self.Stable:set(true,-1)
	self.Incrementing:set(true,-1)
	self.Alarm:set(false, -1)
end

function TM:SetOutputsRunning()
	self.Stable:set(self.IsTickerStable(self.NameOfTicker),-1)
	self.Incrementing:set(self:HasTachCountIncreased(self.Tacho:get_tick()), -1)
	self.Alarm:set(not(self.Stable:get() and self.Incrementing:get()), -1)
end

function TM:IsConveyorRunning()
	return self.Runing and not self.QSP
end

function TM:HasTachCountIncreased(newTickValue)
	if newTickValue == self.oldTickValue then
		return false
	end
	self.oldTickValue = newTickValue
	return true 
end

function TM.IsTickerStable(Ticker)
	local structTable = sm:read("tickermaster")
	for i =0, #structTable.tickers do 
		if structTable.tickers[i].name == Ticker then 
			return (structTable.tickers[i].unstable == 0) --and (structTable.tickers[i].jitter == 0)
		end
	end
	error("Ticker: "..Ticker .. " Not found in sharedmemory")
end

return TM 