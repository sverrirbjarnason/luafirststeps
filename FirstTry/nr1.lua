#!/usr/bin/iolua

local eloop = require('pluto.eloop')

local ljosOn = DIOSignal:new()
local ljosOff = DIOSignal:new()
local ljosWash = DIOSignal:new()

local onButtonInput = DIOSlot:new()
local washButtonInput = DIOSlot:new()

local onButtonOutput = IntegerSignal:new()
local washButtonOutput = IntegerSignal:new()

local onCounter = 0
local washCounter = 0

ljosOn:register("on")
ljosOff:register("off")
ljosWash:register("wash")

onButtonInput:register("onButtonInput")
washButtonInput:register("washButtonInput")

onButtonOutput:register("t1_on_cnt")
washButtonOutput:register("t2_wash_cnt")


ljosOn:set(false)
ljosOff:set(false)
ljosWash:set(false)


function onButtonInput:newstate(state)
    if state ~= ljosOn:get() then
        ljosOn:set(state)
        if state == true then
            print(state)
            onCounter = onCounter + 1
            onButtonOutput:set(onCounter)
        end
        print(onCounter)
    end
end

function washButtonInput:newstate(state)
    if ljosOn:get() == true then
        if state ~= ljosWash:get() then
            ljosWash:set(state)
            if state == true then
                print(state)
                washCounter = washCounter + 1
                washButtonOutput:set(washCounter)
            end
            print(washCounter)
        end
    else 
    print("Mashine must be on")
    end  
end     
