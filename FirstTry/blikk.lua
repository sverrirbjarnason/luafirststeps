#!/usr/bin/iolua

local eloop = require('pluto.eloop')
local ljos = DIOSignal:new()


ljos:register("ljos")
ljos:set(false)

function callback()
    ljos:set(not ljos:get())
   
end

eloop.new_timer(500, true, callback)


