#!/usr/bin/iolua

local eloop = require('pluto.eloop')
math.randomseed(os.time())

local ljosOn = DIOSignal:new()
local ljosOff = DIOSignal:new()
local ljosWash = DIOSignal:new()
local ljosSup = DIOSignal:new()
local ljosOrd = DIOSignal:new()
local ljosReject = DIOSignal:new()
local ljosRejectConveyor = DIOSignal:new()
local ljosMainConveyor = DIOSignal:new()
local ljosSupBin = DIOSignal:new()
local ljosOrdBin = DIOSignal:new()

local onButtonInput = DIOSlot:new()
local washButtonInput = DIOSlot:new()
local supButtonInput = DIOSlot:new()
local ordButtonInput = DIOSlot:new()
local rejectButtonInput = DIOSlot:new()

local supButtonOutput = IntegerSignal:new()
local ordButtonOutput = IntegerSignal:new()
local rejectButtonOutput = IntegerSignal:new()
local supBinOutput = IntegerSignal:new()
local ordBinOutput = IntegerSignal:new()
local supBoxOutput = IntegerSignal:new()
local ordBoxOutput = IntegerSignal:new()

local supCounter = 0
local ordCounter = 0
local rejectCounter = 0
local supBinCounter = 0
local ordBinCounter = 0
local supBoxCounter = 0
local ordBoxCounter = 0
local supEmpty = false
local ordEmpty = false
local grade = math.random(1, 3)

ljosOn:register("on")
ljosOff:register("off")
ljosWash:register("wash")
ljosSup:register("sup")
ljosOrd:register("ord")
ljosReject:register("reject")
ljosRejectConveyor:register("rejectConv")
ljosMainConveyor:register("mainConv")
ljosSupBin:register("supBin")
ljosOrdBin:register("ordBin")

onButtonInput:register("onButtonInput")
washButtonInput:register("washButtonInput")
supButtonInput:register("supButtonInput")
ordButtonInput:register("ordButtonInput")
rejectButtonInput:register("rejectButtonInput")

supButtonOutput:register("sup_cnt")
ordButtonOutput:register("ord_cnt")
rejectButtonOutput:register("reject_cnt")
supBinOutput:register("supBin_cnt")
ordBinOutput:register("ordBin_cnt")
supBoxOutput:register("supBox_cnt")
ordBoxOutput:register("ordBox_cnt")

ljosOn:set(false)
ljosOff:set(false)
ljosWash:set(false)
ljosSup:set(false)
ljosOrd:set(false)
ljosReject:set(false)
ljosRejectConveyor:set(false)
ljosMainConveyor:set(false)
ljosSupBin:set(false)
ljosOrdBin:set(false)

function onButtonInput:newstate(state)
    if state ~= ljosOn:get() then ljosOn:set(state) end
    -- print(ljosOn:get())
end

function washButtonInput:newstate(state)
    if ljosOn:get() == true then
        if state ~= ljosWash:get() then ljosWash:set(state) end
    end
end

function callbackMainConv() ljosMainConveyor:set(not ljosMainConveyor:get()) end

function callbackRejectConv()
    ljosRejectConveyor:set(not ljosRejectConveyor:get())

end

function callbackSupBoxes()
    supBoxCounter = supBoxCounter + 1
    supBoxOutput:set(supBoxCounter)
    supBinCounter = 0
    supEmpty = false
    ljosSupBin:set(supEmpty)
    supBinOutput:set(supBinCounter)
end

function callbackOrdBoxes()
    ordBoxCounter = ordBoxCounter + 1
    ordBoxOutput:set(ordBoxCounter)
    ordBinCounter = 0
    ordEmpty = false
    ljosOrdBin:set(ordEmpty)
    ordBinOutput:set(ordBinCounter)
end

function supBinfunction()
    if supEmpty == false then supEmpty = true end

    if supBinCounter == 4 then
        supBinCounter = 5
        eloop.new_timer(1000, false, function() callbackSupBoxes() end)

    else
        supBinCounter = supBinCounter + 1
        ljosSupBin:set(supEmpty)
    end
    supBinOutput:set(supBinCounter)
end

function ordBinfunction()
    if ordEmpty == false then ordEmpty = true end

    if ordBinCounter == 4 then
        ordBinCounter = 5
        eloop.new_timer(1000, false, function() callbackOrdBoxes() end)

    else
        ordBinCounter = ordBinCounter + 1
        ljosOrdBin:set(ordEmpty)
    end
    ordBinOutput:set(ordBinCounter)
end

function supButtonInput:newstate(state)
    --print("ljosSup1")
    if state ~= ljosSup:get() and ljosOn:get() == true then
        --print("ljosSup")
        ljosSup:set(state)
        if state == true then
            supCounter = supCounter + 1
            supButtonOutput:set(supCounter)
            callbackMainConv()
            eloop.new_timer(2000, false, function()
                callbackMainConv()
                supBinfunction()
            end)
        end
    end
end

function ordButtonInput:newstate(state)
    --print("ljosOrd1")
    if state ~= ljosOrd:get() and ljosOn:get() == true then
        --print("ljosOrd")
        ljosOrd:set(state)
        if state == true then
            ordCounter = ordCounter + 1
            ordButtonOutput:set(ordCounter)
            callbackMainConv()
            eloop.new_timer(2000, false, function()
                callbackMainConv()
                ordBinfunction()
            end)
        end
    end
end

function rejectButtonInput:newstate(state)
    --print("ljosReject1")
    if state ~= ljosReject:get() and ljosOn:get() == true then
        --print("ljosReject")
        ljosReject:set(state)
        if state == true then
            rejectCounter = rejectCounter + 1
            rejectButtonOutput:set(rejectCounter)
            callbackRejectConv()
            eloop.new_timer(2000, false, function()
                callbackRejectConv()
            end)
        end
    end
end

